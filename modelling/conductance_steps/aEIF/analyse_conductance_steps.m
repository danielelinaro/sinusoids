clear all;
close all;
clc;

%%
prefix = 'continuous';
load(['conductance_steps_V_-40.00_gexc_',prefix,'_1.mat']);
t = dt*(0:size(V,2)-1);
N = 5000;
V = V(1:N,:);
gexc = gexc(1:N,:);
ginh = ginh(1:N,:);
spikes = extractSpikes(t,V,VT+10,1);
nspikes = cellfun(@(x) length(x), spikes);
[~,i] = sort(nspikes);

[Vtrimmed,ndx] = removeSpikes(t,V,VT+10,[3e-3,5e-3]);
V_m = nanmean(Vtrimmed);
clear Vtrimmed
gexc_m = mean(gexc);
ginh_m = mean(ginh);
Iexc = gexc.*(E_exc-V);
Iinh = ginh.*(E_inh-V);
clear gexc ginh V
for i=1:length(ndx)
    Iexc(i,ndx{i}) = nan;
end
Iexc_m = nanmean(Iexc);
for i=1:length(ndx)
    Iinh(i,ndx{i}) = nan;
end
Iinh_m = nanmean(Iinh);
I_m = nanmean(Iexc + Iinh);
clear Iexc Iinh

%%
t0 = tpre+tstep;
tlim = [t0-0.05,t0+0.1];
pre = find(t>t0-0.1 & t<t0);
post = find(t>t0 & t<t0+0.1);
[nu,edges] = psth(spikes,1e-3,[tpre,tpre+2*t0]);

figure;

axes('Position',[0.1,0.85,0.25,0.05],'NextPlot','Add');
plot([t0-0.05,t0,t0,t0+0.1],[1,1,1.05,1.05],...
    'Color',[1,.5,0],'LineWidth',2);
placeScaleBars(t0-0.04,1.04,[],0.05,'','5%','k');
axis tight;
axis off;

axes('Position',[0.1,0.6,0.25,0.2],'NextPlot','Add');
plot(t0+[0,0],[0.98,1.1],'--','Color',[.6,.6,.6])
plot(t,gexc_m/mean(gexc_m(pre)),'r');
plot(t,ginh_m/mean(ginh_m(pre)),'b');
axis([tlim,0.95,1.1]);
placeScaleBars(t0-0.04,1.04,0.025,0.05,'25 ms','5%','k');
axis off;

axes('Position',[0.4,0.65,0.5,0.3],'NextPlot','Add');
plot(t0+[0,0],[mean(V_m(pre))-0.5,mean(V_m(pre))+1.5],'--','Color',[.6,.6,.6])
plot(t,V_m,'k');
placeScaleBars(t0-0.02,mean(V_m(pre))+0.2,[],0.5,'','0.5 mV','k');
placeScaleBars(t0+0.05,mean(V_m(pre))-0.2,0.025,[],'25 ms','','k');
axis([tlim,mean(V_m(pre))-0.5,mean(V_m(pre))+1.5]);
axis off;

axes('Position',[0.1,0.45,0.25,0.15],'NextPlot','Add');
plot(t0+[0,0],[mean(Iexc_m(pre))-100,mean(Iexc_m(post))+100],'--','Color',[.6,.6,.6])
plot(t,Iexc_m,'Color',[.5,0,0]);
placeScaleBars(t0+0.05,mean(Iexc_m(post))-100,0.025,[],'25 ms','','k');
placeScaleBars(t0-0.02,mean(Iexc_m(pre))+20,[],100,'','100 pA','k');
axis([tlim,mean(Iexc_m(pre))-100,mean(Iexc_m(post))+100]);
axis off;

axes('Position',[0.1,0.3,0.25,0.15],'NextPlot','Add');
plot(t0+[0,0],[mean(Iinh_m(pre))-100,mean(Iinh_m(post))+100],'--','Color',[.6,.6,.6])
plot(t,Iinh_m,'Color',[0,0,.5]);
placeScaleBars(t0-0.02,mean(Iinh_m(pre))-120,[],100,'','100 pA','k');
axis([tlim,mean(Iinh_m(pre))-100,mean(Iinh_m(post)+100)]);
axis off;

axes('Position',[0.4,0.3,0.5,0.3],'NextPlot','Add');
plot(t0+[0,0],[mean(I_m(pre))-50,mean(I_m(post))+100],'--','Color',[.6,.6,.6])
plot(t,I_m,'Color',[.5,0,.5]);
placeScaleBars(t0-0.02,mean(I_m(pre))+20,[],100,'','100 pA','k');
axis([tlim,mean(I_m(pre))-50,mean(I_m(post)+100)]);
axis off;

axes('Position',[0.1,0.1,0.8,0.15],'NextPlot','Add');
bar(edges,nu,'histc');
plot(t0+[0,0],[0,max(nu(edges>t0))+2],'r','LineWidth',2);
axis([tlim,0,max(nu(edges>t0))+2]);
xlabel('Time (s)');
ylabel('\nu');

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,10]);
print('-depsc2',['conductance_step_',prefix,'.eps']);

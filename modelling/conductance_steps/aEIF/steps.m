clear all;
close all;
clc;

%%
files = {'conductance_steps_V_-40.00_gexc_discontinuous.mat',...
         'conductance_steps_V_-40.00_gexc_continuous.mat',...
         'conductance_steps_V_-40.00_gexc_ginh_discontinuous.mat',...
         'conductance_steps_V_-40.00_gexc_ginh_continuous.mat'};
titles = {'G_{exc} discontinuous','G_{exc} continuous',...
          'G_{exc} & G_{inh} discontinuous','G_{exc} & G_{inh} continuous'};
nfiles = length(files);

for i=1:nfiles
    data = load(files{i},'spikes');
    [nu,edges] = psth(data.spikes,1e-3,[0.1,1.1]);
    subplot(2,2,i);
    bar(edges,nu,'histc');
    title(titles{i});
    axis([0.4,0.8,0,8]);
    if i > 2
        xlabel('Time (s)');
    end
    if mod(i,2)
        ylabel('Frequency (Hz)');
    end
    set(gca,'XTick',0.4:0.2:0.8,'YTick',0:2:8);
    box off;
end
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,12,5]);
print('-depsc2','psths.eps');

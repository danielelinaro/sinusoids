clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;

%%
data = load('conductance_steps_V_-40.00_gexc_continuous.mat','spikes','tpre','tstep');
spikes = data.spikes;
tpre = data.tpre;
tstep = data.tstep;
bin_width = 1e-3;
clear data;
[nu,edges] = psth(spikes,bin_width,[0.1,1.1]);
% bar(edges,nu,'histc');

%%
% After the step, do we observe just a delay, or a decrease in activity? To
% clarify this point, let's compute the probability of detecting a decrease
% in activity where there is no step and compare this to the probability of
% detecting it where the step is actually present.

real_step_time = tpre + tstep;
T = 10e-3;
step_time = real_step_time-50e-3 : 2e-3 : real_step_time+50e-3;
N = 5000;
n_reps = 500;
p = zeros(size(step_time));
for i=1:length(step_time)
    fprintf(1, '[%02d/%d]\n', i, length(step_time));
    p(i) = computeDetectionProbability(spikes, step_time(i), T, N, n_reps, 'decrease');
end
clear i
save('step_control_continuous.mat');

%%
figure;
subplot(2,1,1);
bar(edges,nu,'histc');
axis([0,1.2,ylim]);
xlabel('Time (s)');
ylabel('Frequency (Hz)');
box off;
yl = ylim;
text(1.2,yl(2)-1,sprintf('N_{trials} = %d', length(spikes)),...
    'VerticalAlignment','Bottom','HorizontalAlignment','Right');
text(1.2,yl(2)-3,sprintf('Bin width = %.0f ms', bin_width*1e3),...
    'VerticalAlignment','Bottom','HorizontalAlignment','Right');

subplot(2,1,2);
hold on;
axis([-50,50,0,1]);
plot((step_time-real_step_time)*1e3, p, 'ko', 'MarkerFaceColor', 'k');
plot(xlim,0.5+[0,0],'--','Color',[.6,.6,.6],'LineWidth',2);
text(50,0.5,'Chance level','VerticalAlignment','Bottom','HorizontalAlignment','Right');
text(50,0.9,sprintf('Window duration = %d ms',T*1e3),'VerticalAlignment',...
    'Bottom','HorizontalAlignment','Right');
set(gca,'XTick',-50:10:0,'YTick',0:.5:1);
xlabel('Step time - real step time (ms)');
ylabel('Probability of detection');
set(gcf,'Color','White','PaperUnits','Inch','PaperPosition',[0,0,8,8]);

print('-depsc2','verify_decrease.eps');

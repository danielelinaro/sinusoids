function stepaEIF(gL, V_bal, ntrials)

%%% Parameters of the neuron
C = 281;        % [pF] membrane capacitance
% gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
tauw = 0.144;   % [s] adaptation time constant
a = 4;          % [nS] subthreshold adaptation
b = 0.0805;     % [nA] spike-triggered adaptation
Vr = -55;       % [mV] reset voltage
tarp = 0.01;    % [s] refractory period
Rm = 1e3/gL;

%%% Parameters of the simulated background activity
mode = 'relative';
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;

%%% Parameters of the simulation
dt = 1/20000;   % [s]
tstim = 1;      % [s]
tpre = 0.1;     % [s]
tpost = 0.1;    % [s]
tstep = 0.5;    % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
n = length(t);
ratio = computeRatesRatio(mode,V_bal,Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
[Gm_exc(1),Gm_inh(1),Gs_exc(1),Gs_inh(1)] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);
[Gm_exc(2),Gm_inh(2),Gs_exc(2),Gs_inh(2)] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,R_exc*1.05,ratio,tau_exc*1e3,tau_inh*1e3);

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));

V = zeros(ntrials,n);
gexc = zeros(ntrials,n);
ginh = zeros(ntrials,n);

for k=1:ntrials
    Ge = struct('E', E_exc, 'G', [pre, ...
        OU(tstep-dt,dt,Gm_exc(1),Gs_exc(1),tau_exc,Gm_exc(1),randi(10000)),...
        OU(tstim-tstep,dt,Gm_exc(2),Gs_exc(2),tau_exc,Gm_exc(2),randi(10000)),...
        post]);
    Ge.G(Ge.G<0) = 0;
        %OU(tstep-dt,dt,Gm_inh(1),Gs_inh(1),tau_inh,0,randi(10000)),...
        %OU(tstim-tstep,dt,Gm_inh(2),Gs_inh(2),tau_inh,Gm_inh(1),randi(10000)),...
    Gi = struct('E', E_inh, 'G', [pre, ...
        OU(tstim,dt,Gm_inh(1),Gs_inh(1),tau_inh,Gm_inh(1),randi(10000)),...
        post]);
    Gi.G(Gi.G<0) = 0;
    gexc(k,:) = Ge.G;
    ginh(k,:) = Gi.G;
    V(k,:) = aEIF(tend,dt,C,gL,EL,deltaT,VT,tauw,a,b,Vr,tarp,0,Ge,Gi);
    if mod(k,50) == 0
        fprintf(1, '[%05d/%05d]\n', k, ntrials);
    end
    plot(t,V(k,:),'k');
    pause
end

%%% Pre-analyse the data
spikes = extractAPPeak(t,V,VT+10);
mean_rate = cellfun(@(x) length(x)/tstim, spikes);


cnt = 1;
while 1
    filename = sprintf('conductance_steps_V_%.2f_gexc_discontinuous_%d.mat',V_bal,cnt);
    if ~ exist(filename,'file')
        break;
    end
    cnt = cnt+1;
end
clear t Ge Gi pre post k ntrials cnt
save(filename);


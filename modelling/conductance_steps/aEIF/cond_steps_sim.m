clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;
addpath /home/daniele/MatlabLibrary/neuron_models;

%%
gL = 30;
Vbal = -40;
ntrials = 5000;
stepaEIF(gL, Vbal, ntrials);


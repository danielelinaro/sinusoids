clear all;
close all;
clc;

%%
sym = ['v','o','s','p','^','>','d'];
cols = gray(7*2+10);
cols(:,1) = 1;
cols = cols(6:2:end-5,:);
figure;

%%%% variable number of neurons
g = fittype('a*tanh(b*N)','indep','N','coeff',{'a','b'});
data = load('cond_steps_var_N_increase.mat');
n = length(data.p);
hndl = zeros(n,1);
lgnd = cell(n,1);

axes('Position',[0.1,0.7,0.8,0.25],'NextPlot','Add');
for j=1:n
    m = fit(data.N{j}(:),data.p{j}(:),g,'StartPoint',[1,1e-3]);
    x = linspace(data.N{j}(1),data.N{j}(end),100);
    y = feval(m,x);
    plot(x,y,'k','LineWidth',1,'Color',cols(j,:));
    hndl(j) = plot(data.N{j},data.p{j},['k',sym(j)],'MarkerFaceColor',cols(j,:));
    lgnd{j} = sprintf('T=%.0f ms',data.T(j)*1e3);
end
xlabel('Number of neurons');
ylabel('Probability of detection');
h = legend(hndl,lgnd,'Location','SouthEastOutside');
set(h,'Box','Off');
axis([0,5000,0,1.01]);
set(gca,'XTick',0:1000:5000,'YTick',0:0.5:1, 'TickDir', 'Out');

%%%% variable time since conductance step
cols = gray(7*2+10);
cols(:,3) = 1;
cols = cols(6:2:end-5,:);
g = fittype('1/(1+exp(-(t-t0)/tau))','indep','t','coeff',{'t0','tau'});
data = load('cond_steps_var_T_increase.mat');
n = length(data.p);
hndl = zeros(n,1);
lgnd = cell(n,1);
axes('Position',[0.1,0.35,0.8,0.25],'NextPlot','Add');
for j=1:n
    idx = find(data.T{j}<25e-3);
    data.T{j} = data.T{j}(idx);
    p = data.p{j}(idx);
    m = fit(data.T{j}(:),p(:),g,'StartPoint',[5e-3,10e-3]);
    x = linspace(data.T{j}(1),data.T{j}(end),100);
    y = feval(m,x);
    plot(x*1e3,y,'Color',cols(j,:),'LineWidth',1);
    hndl(j) = plot(data.T{j}*1e3,data.p{j}(idx),['k',sym(j)],'MarkerFaceColor',cols(j,:));
    lgnd{j} = sprintf('N=%d',data.N(j));
end
xlabel('Time since step (ms)');
ylabel('Probability of detection');
h = legend(hndl,lgnd,'Location','SouthEastOutside');
set(h,'Box','Off');
axis([0,25,0,1.01]);
set(gca,'XTick',0:5:25,'YTick',0:0.5:1, 'TickDir', 'Out');

%%%% PSTH
current = load('current_step.mat');
[nu,edges] = psth(data.spikes,1e-3,[0.1,1.6]);
axes('Position',[0.1,0.1,0.8,0.15],'NextPlot','Add');
m = mean(nu(edges>0.6 & edges<1.1));
h = bar(edges*1e3,nu/m,'histc');
set(h,'FaceColor','k','EdgeColor','k');
h = plot(current.t*1e3,1+(current.I-1)*25,'m','LineWidth',2);
h = legend(h,'Current','Location','NorthWest');
set(h,'Box','Off');
axis([900,1300,0,4]);
box off;
set(gca,'XTick',900:100:1400,'XTickLabel',-200:100:300,'YTick',0:4, 'TickDir', 'Out');
xlabel('Time since step (ms)');
ylabel('Normalized firing rate');
    
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,10,7],'PaperSize',[10,7]);
print('-dpdf','cond_steps_model.pdf');


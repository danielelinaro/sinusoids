clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;

%%
% Find the probability of detection of the step as a function of time since
% the application of the stimulus, for different population sizes.
data = load('spikes.mat');
spikes = data.spikeTimes;
step_time = data.duration.pre + data.before*1e-3;
n_reps = 1000;
mode = 'increase';
N = [100, 200, 500, 1000, 2000, 5000, 10000];
T = cell(size(N));
p = cell(size(N));
cnt = 1;
for i=1:length(N)
    T{i} = (1:40)*1e-3;
    total = length(N)*length(T{i});
    p{i} = zeros(size(T{i}));
    for j=1:length(T{i})
        fprintf(1, '[%d/%d]\n', cnt, total);
        p{i}(j) = computeDetectionProbability(spikes, step_time, T{i}(j), N(i), n_reps, mode);
        cnt = cnt+1;
    end
end

save('cond_steps_var_T_increase.mat');

%%
% Find the probability of detection of the step as a function of the number
% of neurons, for different detection times.
step_time = data.duration.pre + data.before*1e-3;
n_reps = 1000;
mode = 'increase';
T = [1,2,5,10,20,50]*1e-3;
N = cell(size(T));
p = cell(size(T));
cnt = 1;
for i=1:length(T)
    N{i} = 100:300:10000;
    total = length(N{i})*length(T);
    p{i} = zeros(size(N{i}));
    for j=1:length(N{i})
        fprintf(1, '[%d/%d]\n', cnt, total);
        p{i}(j) = computeDetectionProbability(spikes, step_time, T(i), N{i}(j), n_reps, mode);
        cnt = cnt+1;
    end
end

save('cond_steps_var_N_increase.mat');

#!/usr/bin/env python

import lcg
import numpy as np
import sys
import os
import getopt
import scipy.io as io
import pylab as p

def runConductanceStepTrial(options,seeds):
    # additional simulation parameters
    dt = h.dt           # [ms]
    after = 100         # [ms]
    # temperature of the simulation
    h.celsius = 36      # [C]
    # a regular-spiking neuron
    n = nrn.makeRS(options['L'],options['diam'])
    area = np.pi*options['diam']*options['L']*1e-8       # [cm^2]
    G = n(0.5).pas.g * area * 1e9  # [nS]
    Rm = 1/(G*1e-3)                # [MOhm}
    #print('Input resistance of the cell: %g MOhm.' % Rm)
    # background synaptic activity parameters
    gexc = {'tau': 5, 'E': 0}
    ginh = {'tau': 10, 'E': -80}
    ratio = lcg.computeRatesRatio(options['Vbal'], Rin=Rm, tau_exc=gexc['tau'], tau_inh=ginh['tau'], E_exc=gexc['E'], E_inh=ginh['E'])
    #print('The ratio of excitatory rate to inhibitory rate is %g:1.' % ratio)
    gexc['mu_pre'],ginh['mu'],gexc['sigma_pre'],ginh['sigma'] = \
        lcg.computeSynapticBackgroundCoefficients(ratio, options['R_exc'], Rin=Rm, tau_exc=gexc['tau'], tau_inh=ginh['tau'])
    gexc['mu_post'] = gexc['mu_pre'] * (1+options['dR_exc'])
    gexc['sigma_post'] = gexc['sigma_pre'] * np.sqrt(1+options['dR_exc'])
    #print('mean of excitatory conductance before the step: %g nS' % gexc['mu_pre'])
    #print('stddev of excitatory conductance before the step: %g nS' % gexc['sigma_pre'])
    #print('mean of excitatory conductance after the step: %g nS' % gexc['mu_post'])
    #print('stddev of excitatory conductance after the step: %g nS' % gexc['sigma_post'])
    #print('mean of inhibitory conductance: %g nS' % ginh['mu'])
    #print('stddev of inhibitory conductance: %g nS' % ginh['sigma'])
    # the OU processes that simulate the backgroun noisy activity
    gexc['stim_pre'],gexc['vec_pre'] = nrn.makeNoisyGclamp(n(0.5), options['duration']['pre'], dt, gexc['mu_pre'], gexc['sigma_pre'], \
                                                               gexc['tau'], gexc['E'], delay=options['before'], seed=seeds[0])
    gexc['stim_post'],gexc['vec_post'] = nrn.makeNoisyGclamp(n(0.5), options['duration']['post'], dt, gexc['mu_post'], gexc['sigma_post'], \
                                                                 gexc['tau'], gexc['E'], delay=options['before']+options['duration']['pre'], seed=seeds[1])
    ginh['stim'],ginh['vec'] = nrn.makeNoisyGclamp(n(0.5), options['duration']['pre']+options['duration']['post'], dt, ginh['mu'], ginh['sigma'], \
                                                       ginh['tau'], ginh['E'], delay=options['before'], seed=seeds[2])
    # recorders
    rec = nrn.makeRecorders(n(0.5), {'v':'_ref_v'})
    apc = nrn.makeAPcount(n(0.5))
    spikes = h.Vector()
    apc.record(spikes)
    # run the simulation
    nrn.run(options['duration']['pre']+options['duration']['post']+options['before']+after)

    chunk_dur = 200
    n = chunk_dur/h.dt
    Gexc = np.zeros(2*n)
    Gexc[:n] = np.array(gexc['vec_pre'])[-n:]
    Gexc[n:] = np.array(gexc['vec_post'])[:n]
    start = np.where(np.array(rec['t']) > options['before']+options['duration']['pre']-dt)[0][0]
    V = np.array(rec['v'])[start-n:start+n]
    Ginh = np.array(ginh['vec'])[start-n:start+n]
    #T = (np.arange(2*n)*dt - chunk_dur)*1e-3
    #p.plot(T,V,'k')
    #p.plot(T,Gexc,'r')
    #p.show()
    return np.array(spikes),V,Gexc,Ginh

def usage():
    print('This script performs the injection of an excitatory conductance step into a model neuron.')
    print('')
    print('Usage: %s [--option <value>]' % os.path.basename(sys.argv[0]))
    print('')
    print('where options are:')
    print('')
    print(' -h, --help        Display this help message and exit.')
    print(' -o, --output      Save data to the specified file (default \'spikes.mat\').')
    print(' -v, --voltage     Balanced voltage (default -35 mV).')
    print(' -s, --step        Percentage of increase in the excitatory conductance (default 5%).')
    print(' -n, --ntrials     Number of trials (default 10000).')
    print('')

def main():
    try:
        opts,args = getopt.getopt(sys.argv[1:],'ho:v:s:n:',['help','output=','voltage=','step=','ntrials='])
    except getopt.GetoptError, err:
        print(str(err))
        usage()
        sys.exit(1)

    options = {
        # neuron parameters
        'L': 80.,       # [um]
        'diam': 80.,    # [um]
        # background synaptic activity parameters
        'R_exc': 7000.,       # [Hz]
        'dR_exc': 0.05,       # [1]
        'Vbal': -35.,         # [mV]
        # duration of the simulation
        'duration': {'pre': 1000., 'post': 500.},   # [ms]
        'before': 100.        # [ms]
        }

    ntrials = 10000
    outfile = 'spikes.mat'

    for o,a in opts:
        if o in ('-h','--help'):
            usage()
            sys.exit(0)
        elif o in ('-o','--output'):
            outfile = a
        elif o in ('-v','--voltage'):
            options['Vbal'] = float(a)
            if options['Vbal'] < -80 or options['Vbal'] > 0:
                print('The balanced voltage must be between -80 and 0 mV')
                sys.exit(1)
        elif o in ('-s','--step'):
            options['dR_exc'] = float(a)/100
        elif o in ('-n','--ntrials'):
            ntrials = int(a)
            if ntrials <= 0:
                print('The number of trials must be positive.')
                sys.exit(1)
    
    global h
    global nrn
    from neuron import h
    import nrnutils as nrn

    data = options
    spikes = []
    for k in range(ntrials):
        spks,v,gexc,ginh = runConductanceStepTrial(options,[k*3+1,k*3+2,(k+1)*3])
        spikes.append(list(spks*1e-3))
        if k == 0:
            data['V'] = np.zeros((ntrials,len(v)))
            data['Gexc'] = np.zeros((ntrials,len(gexc)))
            data['Ginh'] = np.zeros((ntrials,len(ginh)))
        data['V'][k,:] = v
        data['Gexc'][k,:] = gexc
        data['Ginh'][k,:] = ginh
        if (k+1)%(ntrials/10) == 0:
            sys.stdout.write('[%d/%d]\n' % (k+1,ntrials))
            sys.stdout.flush()

    data['spikeTimes'] = spikes
    data['duration'].update((x,y*1e-3) for x,y in data['duration'].items())
    data['dt'] = h.dt
    io.savemat(outfile,data,do_compression=True,oned_as='column')

if __name__ == '__main__':
    main()

clear all;
close all;
clc;

%%
tau = [0.1,1,10,100]*1e-3; % [s]
A = 1;                     % [au]
dt = 1/50000;              % [s]
tend = 100;                % [s]
F = 3000;                  % [Hz]
n = round(tend/dt);
y = zeros(n,length(tau));
for i=1:length(tau)
    y(:,i) = stein(tend, A, tau(i), F, dt);
end
t = (0:n-1)' * dt;

%%
Fs = 1/dt;
f = (0:Fs/n:Fs/2)';
xdft = fft(y);
xdft = xdft(1:n/2+1,:);
psdx = 1/(Fs*n) * abs(xdft).^2;
psdx(2:end-1,:) = 2*psdx(2:end-1,:);

%%
cmap = lines(size(psdx,2));
figure;
for i=1:length(tau)    
    subplot(2,2,i);
    hold on;
    plot(f,10*log10(psdx(:,i)),'Color',min([1,1,1],cmap(i,:)+[.5,.5,.5]));
    plot(f,10*log10(A^2*tau(i)^2*F./(1+(2*pi*f*tau(i)).^2)),'Color',...
        cmap(i,:),'LineWidth',2);
    title(sprintf('\\tau = %g ms', tau(i)*1e3));
    set(gca,'XScale','Log','XTick',logspace(0,4,5));
    axis([f([1,end])',-200,50]);
    if i>2
        xlabel('Frequency (Hz)');
    end
    if mod(i,2)
        ylabel('PSD (dB/Hz)');
    end
end

set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',[0,0,8,8]);
print('-depsc2','stein.eps');

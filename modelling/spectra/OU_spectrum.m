clear all;
close all;
clc;
addpath /Users/daniele/Postdoc/Research/MatlabLibrary;
addpath /Users/daniele/Postdoc/Research/MatlabLibrary/neuron_models;

%%
gL = 30;        % [nS] leak conductance
Rm = 1e3/gL;
V_bal = -40;    % [mV] balanced voltage

%%% Parameters of the simulated background activity
mode = 'relative';
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;
dR = 0.1;

%%% Parameters of the simulation
dt = 1e-6;      % [s]
tend = 5;       % [s]
t = (dt:dt:tend)';
ratio = computeRatesRatio(mode,V_bal,Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
[Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);

F = [1,10,100,1000];
nF = length(F);
n = length(t);
N = 3;
G = zeros(n,nF*N);
for i=1:N:N*nF
    mu = Gm_exc*(1+dR*sin(2*pi*F((i+N-1)/N)*t));
    sigma = Gs_exc*sqrt(1+dR*sin(2*pi*F((i+N-1)/N)*t));
    seed = randi(10000);
    G(:,i) = OU(tend-dt,dt,mu,sigma,tau_exc,0,seed);
    G(:,i+1) = mu + sigma.*OU(tend-dt,dt,0,1,tau_exc,0,seed)';
    G(:,i+2) = mu + OU(tend-dt,dt,0,sigma,tau_exc,0,seed)';
end

%% Compute the PSD
Fs = 1/dt;
f = (0:Fs/n:Fs/2)';
xdft = fft(G);
xdft = xdft(1:n/2+1,:);
psdx = (1/(Fs*n)).*abs(xdft).^2;
psdx(2:end-1,:) = 2*psdx(2:end-1,:);

%%
figure;
cmap = lines(n);
lgnd = {'Filtered','Instantaneous','Hybrid'};
hndl = zeros(3,1);
idx = find(t>0.1&t<0.12);
for i=1:N:N*nF
    subplot(ceil(nF/2),min(2,nF),(i+N-1)/N);
    hold on;
    hndl(2) = plot(t(idx)*1e3,G(idx,i+1),'Color',cmap(2,:));    % instantaneous mean and variance
    hndl(3) = plot(t(idx)*1e3,G(idx,i+2),'Color',cmap(1,:));    % hybrid
    hndl(1) = plot(t(idx)*1e3,G(idx,i),'Color',cmap(3,:));      % filtered mean and variance
    axis([100,120,Gm_exc-4*Gs_exc,Gm_exc+4*Gs_exc]);
    title(sprintf('F = %.0f Hz', F((i+N-1)/N)));
    if i == 1
        legend(hndl,lgnd,'Location','SouthEast');
    end
    if mod((i+N-1)/N,2)
        ylabel('OU (a.u.)');
        set(gca,'YTick',15:5:30);
    else
        set(gca,'YTick',[]);
    end    
    if (i+N-1)/N > ceil(nF/2)
        xlabel('Time (ms)');
        set(gca,'XTick',100:10:120);
    else
        set(gca,'XTick',[]);
    end
end
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',...
    [0,0,ceil(nF/2)*4,min(2,nF)*4]);
print('-depsc2','OU_vs_t.eps');

%%
figure;
cmap = lines(n);
lgnd = {'Filtered','Instantaneous','Hybrid','Theory'};
hndl = zeros(4,1);
for i=1:N:N*nF
    subplot(ceil(nF/2),min(2,nF),(i+N-1)/N);
    hold on;
    hndl(2) = plot(f,20*log10(psdx(:,i+1)),'Color',cmap(2,:));    % instantaneous mean and variance
    hndl(3) = plot(f,20*log10(psdx(:,i+2)),'Color',cmap(1,:));    % hybrid
    hndl(1) = plot(f,20*log10(psdx(:,i)),'Color',cmap(3,:));      % filtered mean and variance
    hndl(4) = plot(f,20*log10(2*tau_exc*Gs_exc^2./(1+(2*pi*f*tau_exc).^2)),'k','LineWidth',2);
    axis([f(1),1e4,-200,50]);
    set(gca,'XScale','Log');
    title(sprintf('F = %.0f Hz', F((i+N-1)/N)));
    if i == 1
        legend(hndl,lgnd,'Location','SouthWest');
    end
    if mod((i+N-1)/N,2)
        ylabel('PSD (dB)');
        set(gca,'YTick',-200:50:50);
    else
        set(gca,'YTick',[]);
    end    
    if (i+N-1)/N > ceil(nF/2)
        xlabel('Frequency (Hz)');
        set(gca,'XTick',logspace(0,4,5));
    else
        set(gca,'XTick',[]);
    end

end
set(gcf,'Color','w','PaperUnits','Inch','PaperPosition',...
    [0,0,ceil(nF/2)*4,min(2,nF)*4]);
print('-depsc2','OU_vs_f.eps');

clear all;
close all;
clc;

%%
tau = [0.1,1,10,100];      % [ms]
sigma = 1;                 % [au]
c = 2*sigma.^2./tau;
dt = 0.005;                % [ms]
tend = 1000;               % [ms]
t = 0:dt:tend;
n = length(t);
eta = zeros(length(tau)*length(sigma),n);

for i=1:length(tau)
    mu = exp(-dt/tau(i));
    for j=1:length(sigma)
        noise = sqrt(c(j)*tau(j)/2 * (1-mu^2)) * randn(n,1);
        for k=1:n-1
            eta((i-1)*length(sigma)+j,k+1) = mu*eta((i-1)*length(sigma)+j,k) + noise(k);
        end
    end
end

%%
Fs = 1e3/dt;
NFFT = 2^nextpow2(n);
Y = fft(eta',NFFT)/n;
Z = 2*abs(Y(1:NFFT/2+1,:));
Y = Y';
Z = Z';
f = Fs/2*linspace(0,1,NFFT/2+1);

%%
cmap = jet(size(Y,1));
hndl = zeros(size(tau));
lgnd = cell(size(tau));
figure;
hold on;
for i=length(tau):-1:1
    hndl(i) = plot(f,Z(i,:),'Color',cmap(i,:));
    plot(1e3/(2*pi*tau(i))+[0,0],[1e-8,1e0],'--','Color',cmap(i,:),'LineWidth',2);
    lgnd{i} = sprintf('\\tau=%.1f ms -> f_c=%.0f Hz', tau(i), 1e3/(2*pi*tau(i)));
end
legend(hndl,lgnd,'Location','SouthWest');
set(gca,'XScale','Log','YScale','Log');
axis([f([1,end]),1e-8,1e0]);

clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;
addpath /home/daniele/MatlabLibrary/neuron_models;

%%
ntrials = 1000;
Fs = 20000;
dt = 1/Fs;
tau = 10e-3;
t0 = 0.1;
dur = 0.45;
t = 0:dt:dur;
G0 = 0.2;
R = 7000;
Gm0 = G0*R*tau;
Gm1 = G0*(1.05*R)*tau;
Gs0 = G0*sqrt(R*tau/2);
Gs1 = G0*sqrt((1.05*R)*tau/2);
G = [zeros(1,round(t0/dt)), ...
     Gm0*(1-exp(-t/tau)), ...
     Gm1*(1-exp(-t/tau)) + Gm0*exp(-t/tau)];
nsamples = length(G);
t = (0:nsamples-1)*dt;
g = zeros(ntrials,nsamples);
for i=1:ntrials
    a = OU(dur,dt,Gm0,Gs0,tau,0);
    b = OU(dur,dt,Gm1,Gs1,tau,a(end));
    g(i,:) = [zeros(1,round(t0/dt)), a, b];
    g(i,g(i,:)<0) = 0;
end

%%
figure;
hold on;
plot(t,G,'r','LineWidth',2);
plot(t,g(1,:),'Color',[.6,.6,.6])
plot(t,mean(g),'k');
hndl = legend('Theoretical mean','Single realization',...
    sprintf('Mean of %d realizations',ntrials),'Location','SouthEast');
set(hndl,'box','off');
xlabel('Time (s)');
ylabel('Conductance (nS)');
box off;
axis([0,1,0,1.1*max(g(1,:))]);
set(gca,'xtick',0:.5:1,'ytick',0:5:20);
set(gcf,'Color','w','PaperUnits','inch','PaperPosition',[0,0,6,4]);
% print('-depsc2','test_OU.eps');

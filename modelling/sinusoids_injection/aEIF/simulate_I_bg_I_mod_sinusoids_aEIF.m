function simulate_I_bg_I_mod_sinusoids_aEIF(Imod, Im, Is, Itau, outfilename)
% simulate_I_bg_I_mod_sinusoids_aEIF(Imod, Im, Is, Itau, outfilename)

%%% Parameters of the neuron
C = 281;        % [pF] membrane capacitance
gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
tauw = 0.144;   % [s] adaptation time constant
a = 4;          % [nS] subthreshold adaptation
b = 0.0805;     % [nA] spike-triggered adaptation
Vr = -55;       % [mV] reset voltage
tarp = 0.004;   % [s] refractory period

if ~ exist('Imod','var')
    Imod = 100;        % [pA] modulating current
end
% low spiking frequency
if ~ exist('Im','var')
    Im = 450;       % [pA] mean of the background current
end
if ~ exist('Is','var')
    Is = 200;       % [pA] standard deviation of the background current
end
if ~ exist('Itau','var')
    Itau = 20e-3;   % [s] time constant of the background current
end

%%% Parameters of the simulation
dt = 1/20000;   % [s]
tstim = 200;    % [s]
tpre = 1;       % [s]
tpost = 1;      % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));

F = logspace(0,3,50);
nF = length(F);
tpeak = cell(nF,1);
spikeTimes = cell(nF,1);

for i=1:nF
    I = OU(tend,dt,Im,Is,Itau,Im,randi(10000)) + Imod*sin(2*pi*F(i)*(t-tpre));
    I(1:length(pre)) = 0;
    I(end-length(post)+1:end) = 0;
    fprintf(1, '[%02d/%02d] >> F = %g\n', i, nF, F(i));
    V = aEIF(tend,dt,C,gL,EL,deltaT,VT,tauw,a,b,Vr,tarp,I);
    spks = extractAPPeak(t,V,VT+10);
    tpeak{i} = spks{1};
    spks = extractSpikes(t,V,VT+10,1);
    spikeTimes{i} = spks{1};
end

cnt = 1;
filename = outfilename;
while 1
    if ~exist(filename,'file')
        break;
    end
    filename = sprintf('%s_%d.mat',outfilename(1:end-4),cnt);
    cnt = cnt+1;
end
clear pre post t V I i cnt spks

save(filename);


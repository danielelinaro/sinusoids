function simulate_G_bg_I_mod_sinusoids_aEIF(V_bal, outfilename)
%  simulate_G_bg_I_mod_sinusoids_aEIF(V_bal, outfilename)

%%% Parameters of the neuron
C = 281;        % [pF] membrane capacitance
gL = 30;        % [nS] leak conductance
EL = -70.6;     % [mV] leak reversal potential
VT = -50.4;     % [mV] spike threshold
deltaT = 2;     % [mV] slope factor
tauw = 0.144;   % [s] adaptation time constant
a = 4;          % [nS] subthreshold adaptation
b = 0.0805;     % [nA] spike-triggered adaptation
Vr = -55;       % [mV] reset voltage
tarp = 0.004;   % [s] refractory period
Rm = 1e3/gL;
I0 = 0;          % [pA] baseling current
I1 = 75;        % [pA] modulating current

%%% Parameters of the simulated background activity
mode = 'relative';
tau_exc = 5e-3;
tau_inh = 10e-3;
E_exc = 0;
E_inh = -80;
R_exc = 7000;

%%% Parameters of the simulation
dt = 1/20000;   % [s]
tstim = 200;    % [s]
tpre = 1;       % [s]
tpost = 1;      % [s]
tend = tstim + tpre + tpost;
t = 0:dt:tend;
ratio = computeRatesRatio(mode,V_bal,Rm,tau_exc*1e3,tau_inh*1e3,E_exc,E_inh);
[Gm_exc,Gm_inh,Gs_exc,Gs_inh] = ...
    computeSynapticBackgroundCoefficients(mode,Rm,R_exc,ratio,tau_exc*1e3,tau_inh*1e3);

%%% Simulate the neuron
pre = zeros(1,round(tpre/dt));
post = zeros(1,round(tpost/dt));

F = logspace(0,3,50);
nF = length(F);
tpeak = cell(nF,1);
spikeTimes = cell(nF,1);

for i=1:nF
    I = I0 + I1*sin(2*pi*F(i)*(t-tpre));
    I(1:length(pre)) = 0;
    I(end-length(post)+1:end) = 0;
    Ge = struct('E',E_exc,'G',[pre,OU(tstim,dt,Gm_exc,Gs_exc,tau_exc,Gm_exc,randi(10000)),post]);
    Ge.G(Ge.G<0) = 0;
    Gi = struct('E',E_inh,'G',[pre,OU(tstim,dt,Gm_inh,Gs_inh,tau_inh,Gm_inh,randi(10000)),post]);
    Gi.G(Gi.G<0) = 0;
    fprintf(1, '[%02d/%02d] >> F = %g\n', i, nF, F(i));
    V = aEIF(tend,dt,C,gL,EL,deltaT,VT,tauw,a,b,Vr,tarp,I,Ge,Gi);
    spks = extractAPPeak(t,V,VT+10);
    tpeak{i} = spks{1};
    spks = extractSpikes(t,V,VT+10,1);
    spikeTimes{i} = spks{1};
end

cnt = 1;
filename = outfilename;
while 1
    if ~exist(filename,'file')
        break;
    end
    filename = sprintf('%s_%d.mat',outfilename(1:end-4),cnt);
    cnt = cnt+1;
end
clear pre post gexc ginh t V Ge Gi I n mode ratio i cnt spks

save(filename);


clear all;
close all;
clc;
addpath /home/daniele/MatlabLibrary;
addpath /home/daniele/MatlabLibrary/neuron_models;

%% Current background + current modulation
% Imod = 100;
% Itau = 0.02;
%%% low firing frequency
% Im = 450;
% Is = 200;
%%% high firing frequency
% Im = 550;
% Is = 220;
% simulate_I_bg_I_mod_sinusoids_aEIF(Imod, Im, Is, Itau, 'sinusoids_I_bg_I_mod_aEIF_high_freq.mat');

%% Conductance background + current modulation
%%% low firing frequency
% Vbal = -38;
%%% high firing frequency
% Vbal = -33;
% simulate_G_bg_I_mod_sinusoids_aEIF(Vbal, 'sinusoids_G_bg_I_mod_aEIF_high_freq.mat');

%% Conductance background + conductance modulation
%%% low firing frequency
% Vbal = -38;
%%% high firing frequency
Vbal = -33;
simulate_G_bg_G_mod_sinusoids_aEIF(Vbal, 'sinusoids_G_bg_G_mod_aEIF_high_freq.mat');


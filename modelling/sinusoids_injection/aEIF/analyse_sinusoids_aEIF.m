function analyse_sinusoids_aEIF(filename, do_plot)

if ~ exist('do_plot','var')
    do_plot = 1;
end

in_filename = filename;
load(in_filename);

ttran = 1;
stimulusDuration = zeros(nF,1);

% remove the time at which the simulation started
for k=1:nF
    % compute the correct transient time and remove the preceding spikes
    tr = round(ttran*F(k))/F(k);
    if tr ~= ttran
        fprintf(1, ['Using transient equal to %g seconds ', ...
            '(rounded from %g seconds for a frequency of %g Hz).\n'], tr, ttran, F(k));
    end
    stimulusDuration(k) = tstim - tr;
    tr = tr + tpre;
    % spike times minus the transient duration: note that the spike times
    % are also relative to the beginning of the sinusoidal modulation
    spikeTimes{k} = spikeTimes{k}(spikeTimes{k} > tr) - tr;
end

%%% extract the sinusoidal modulation from the original spike times
nBins = 10;
nPeriods = 1;
fitMeanRate = 1;
r0 = zeros(nF,1);
r1 = zeros(nF,1);
phi = zeros(nF,1);

ncol = ceil(nF/5);
if do_plot
    figure;
    subplot(5,ncol,1);
end
[r0(1),r1(1),phi(1),conf] = extractSinusoidalModulation(spikeTimes{1}, ...
                        F(1), nBins, stimulusDuration(1), nPeriods, fitMeanRate, do_plot);
conf = repmat(conf, [nF,1]);

for k=2:nF
    if do_plot
        subplot(5,ncol,k);
    end
    [r0(k),r1(k),phi(k),conf(k)] = extractSinusoidalModulation(...
            spikeTimes{k}, F(k), nBins, stimulusDuration(k), nPeriods, fitMeanRate, do_plot);
end
if do_plot
    set(gcf, 'Color', [1,1,1], 'PaperUnits', 'Inch', 'PaperPosition', [0,0,12,6]);
    print('-depsc2','fit_histograms.eps');
end

% adjust the values of phi to have them in a nice looking way when plotted
phi = adjustPhase(F,phi,pi/2,do_plot);

%%% extract the sinusoidal modulation from the shuffled spike times
shuffle_iter = 50;
spikeTimesShuffled = cell(nF,1);

r0_shuffled = zeros(nF,shuffle_iter);
r1_shuffled = zeros(nF,shuffle_iter);
phi_shuffled = zeros(nF,shuffle_iter);

fprintf(1, 'Shuffling the spike times %d times ', shuffle_iter);
for i=1:shuffle_iter
    % shuffle the spike times
    for j=1:nF
        isi = diff(spikeTimes{j});
        idx = randperm(length(isi));
        spikeTimesShuffled{j} = cumsum(isi(idx));
    end

    [r0_shuffled(1,i),r1_shuffled(1,i),phi_shuffled(1,i)] = ...
        extractSinusoidalModulation(spikeTimesShuffled{1}, ...
        F(1), nBins, stimulusDuration(1), nPeriods, fitMeanRate, 0);

    for j=2:nF
        [r0_shuffled(j,i),r1_shuffled(j,i),phi_shuffled(j,i)] = ...
            extractSinusoidalModulation(spikeTimesShuffled{j}, ...
            F(j), nBins, stimulusDuration(j), nPeriods, fitMeanRate, 0);
    end
    fprintf(1, '.');
end
fprintf(1, ' done.\n');

r0_shuffled = mean(r0_shuffled,2);
r1_shuffled = mean(r1_shuffled,2);
phi_shuffled = mean(phi_shuffled,2);

% fit a rational transfer function
idx = find(F<=1000);
w = [diff(reshape([conf.r1],[2,length(conf)]))', diff(reshape([conf.phi],[2,length(conf)]))'];
[A,Z,P,Dt,offset] = fitTransferFunction(F(idx), r1(idx)./r0(idx), phi(idx), 0, 1, 1./w(idx,:), [], do_plot);
print('-depsc2','fit_transfer_function.eps');
TF = struct('A',A,'Z',Z,'P',P,'Dt',Dt,'offset',offset);

if do_plot
    figure;
    subplot(2,1,1);
    loglog(F,abs(r1)./r0,'ko-','LineWidth',2,'MarkerFaceColor','k','MarkerSize',4);
    hold on;
    loglog(F,abs(r1_shuffled)./r0_shuffled,'bo','LineWidth',2,'MarkerFaceColor','k','MarkerSize',4);
    ylabel('r_1/r_0');
    legend('Real spike times', 'Shuffled spike times','Location','SouthWest');
    subplot(2,1,2);
    semilogx(F,180/pi*phi,'ko-','LineWidth',2,'MarkerFaceColor','k','MarkerSize',4);
    xlabel('F (Hz)');
    ylabel('\Phi');
    set(gcf,'Color',[1,1,1],'PaperUnits','Inch','PaperPosition',[0,0,8,8]);
    print('-depsc2','modulation_phase_shift_ap_peak.eps');
end

save([in_filename(1:end-4),'_analysed.mat']);

end

function phi_out = adjustPhase(F,phi_in,tolerance,do_plot)
n = length(phi_in);
phi_out = phi_in;
for k=2:n
    while abs(phi_out(k)-phi_out(k-1)) >= pi
        if phi_out(k) > phi_out(k-1)
            phi_out(k) = phi_out(k)-2*pi;
        else
            phi_out(k) = phi_out(k)+2*pi;
        end
    end
end
idx = find(F >= 100);
start = find(phi_out(idx) < 0, 1);
for k=start+1:length(idx)
    while phi_out(idx(k)) > phi_out(idx(k-1))+tolerance
        phi_out(idx(k)) = phi_out(idx(k)) - 2*pi;
    end
end
if do_plot
    figure;
    semilogx(F,phi_in,'ko-','MarkerFaceColor','k');
    hold on;
    semilogx(F,phi_out,'ro-','MarkerFaceColor','r');
    box off;
    xlabel('Frequency (Hz)');
    ylabel('Phase (rad/s)');
    set(gcf,'Color','w');
end
end

#!/usr/bin/env python

from neuron import h
import nrnutils as nrn
import lcg
import numpy as np
h.load_file('stdlib.hoc')

ttran = 2000

def taper_section(sec, max_diam, min_diam):
    if max_diam < 0 or min_diam < 0:
        raise Exception('Diameters must be positive')
    for seg in sec:
        seg.diam = max_diam - seg.x * (max_diam - min_diam)

def compute_section_area(section):
    a = 0.
    for segment in section:
        a += h.area(segment.x, sec=section)
    return a

class Neuron:
    def __init__(self, L, diam, with_axon=False):
        self.soma = h.Section(name='soma')
        self.soma.Ra = 100
        self.soma.nseg = 1
        self.soma.L = L
        self.soma.diam = diam
        self.soma.cm = 1
        self.soma.insert('pas')
        self.soma.e_pas = -70
        self.soma.g_pas = 0.0001
        self.with_axon = with_axon
        if self.with_axon:
            self.axon = {}
            for sec in ('hillock','AIS','axon'):
                self.axon[sec] = h.Section(name=sec)
                self.axon[sec].cm = 1
                self.axon[sec].Ra = 100
                self.axon[sec].insert('pas')
                self.axon[sec].e_pas = -70
                self.axon[sec].g_pas = 0.0001
            self.axon['hillock'].L = 20
            self.axon['hillock'].diam = 3.5
            self.axon['hillock'].nseg = 5
            self.axon['AIS'].L = 25
            self.axon['AIS'].diam = 2
            self.axon['AIS'].nseg = 5
            self.axon['axon'].L = 500
            self.axon['axon'].diam = 1.5
            self.axon['axon'].nseg = int((self.axon['axon'].L/(0.1*h.lambda_f(100,sec=self.axon['axon']))+0.9)/2)*2 + 1
            taper_section(self.axon['hillock'], 3.5, 2.)
            taper_section(self.axon['AIS'], 2., 1.5)
            self.axon['hillock'].connect(self.soma, 1, 0)
            self.axon['AIS'].connect(self.axon['hillock'], 1, 0)
            self.axon['axon'].connect(self.axon['AIS'], 1, 0)

    def Rm(self):
        G = self.soma.g_pas * compute_section_area(self.soma) * 10  # [nS]
        if self.with_axon:
            for sec in self.axon.values():
                G += (sec.g_pas * compute_section_area(sec) * 10)
        return 1. / (G*1e-3)

class RegularSpikingNeuron (Neuron):
    def __init__(self, L, diam, with_axon=False):
        Neuron.__init__(self, L, diam, with_axon)
        self.soma.insert('hh2')
        self.soma.ek = -100
        self.soma.ena = 50
        self.soma.vtraub_hh2 = -55
        self.soma.gnabar_hh2 = 0.05
        self.soma.gkbar_hh2 = 0.005
        if self.with_axon:
            for sec in ('hillock','AIS','axon'):
                self.axon[sec].insert('hh2')
                self.axon[sec].ek = -100
                self.axon[sec].ena = 50
                self.axon[sec].vtraub_hh2 = -65
                self.axon[sec].gkbar_hh2 = self.soma.gkbar_hh2
            self.axon['hillock'].gnabar_hh2 = 10*self.soma.gkbar_hh2
            self.axon['AIS'].gnabar_hh2 = 20*self.soma.gkbar_hh2
            self.axon['axon'].gnabar_hh2 = self.soma.gkbar_hh2

class AdaptingNeuron (RegularSpikingNeuron):
    def __init__(self, L, diam, with_axon=False):
        RegularSpikingNeuron.__init__(self, L, diam, with_axon)
        self.soma.insert('im')
        h.taumax_im = 1000
        self.soma.gkbar_im = 7e-5

def saveMatlabFile(filename, spikes, tpre, tstim):
    import scipy.io as io
    CV = []
    F = []
    meanRate = []
    spikeTimes = []
    stimulusDurations = [(tstim-ttran)*1e-3 for i in range(len(spikes))]
    for k,v in spikes.iteritems():
        isi = np.diff(v*1e-3)
        CV.append(np.std(isi)/np.mean(isi))
        F.append(k)
        meanRate.append(len(v)/stimulusDurations[0])
        spikeTimes.append(list(v*1e-3))
    matlab_data = {'spikeTimes': spikeTimes, 'F': F, 'CV': CV, 'meanRate': meanRate, 'stimulusDurations': stimulusDurations,
                   'tstim': tstim*1e-3, 'tpre': tpre*1e-3}
    io.savemat(filename,matlab_data,do_compression=True,oned_as='column')

def makeSine(segment, dur, dt, Imu, Isigma, Imod, f, phi, delay):
    t = (np.arange(0,dur+delay+1.5*dt,dt) - delay)*1e-3
    I = nrn.OU(Imu,Isigma,5,dur+delay+dt,dt,5061983) + Imod*np.sin(2*np.pi*f*t + phi)
    I[t<0] = 0
    I[-1] = 0
    vec = h.Vector(I)
    stim = h.IClamp(segment)
    stim.dur = dur
    stim.delay = delay
    vec.play(stim._ref_amp,dt)
    return stim,vec

def runCurrentModulationTrialWithConductanceBG(n, R_exc, Vbal, I0, I1, f, dur, before):
    import numpy.random as rnd
    # additional simulation parameters
    dt = h.dt           # [ms]
    after = 100         # [ms]
    # temperature of the simulation
    h.celsius = 36      # [C]
    print('Input resistance of the cell: %g MOhm.' % n.Rm())
    # background synaptic activity parameters
    gexc = {'tau': 5, 'E': 0}
    ginh = {'tau': 10, 'E': -80}
    ratio = lcg.computeRatesRatio(Vbal, Rin=n.Rm(), tau_exc=gexc['tau'], tau_inh=ginh['tau'], E_exc=gexc['E'], E_inh=ginh['E'])
    print('The ratio of excitatory rate to inhibitory rate is %g:1.' % ratio)
    gexc['mu'],ginh['mu'],gexc['sigma'],ginh['sigma'] = \
        lcg.computeSynapticBackgroundCoefficients(ratio, R_exc, Rin=n.Rm(), tau_exc=gexc['tau'], tau_inh=ginh['tau'])
    print('mean of excitatory conductance: %g nS' % gexc['mu'])
    print('stddev of excitatory conductance: %g nS' % gexc['sigma'])
    print('mean of inhibitory conductance: %g nS' % ginh['mu'])
    print('stddev of inhibitory conductance: %g nS' % ginh['sigma'])
    # the OU processes that simulate the backgroun noisy activity
    gexc['stim'],gexc['vec'] = nrn.makeNoisyGclamp(n.soma(0.5), dur, dt, gexc['mu'], gexc['sigma'], gexc['tau'], gexc['E'], delay=before, seed=rnd.poisson(10000))
    ginh['stim'],ginh['vec'] = nrn.makeNoisyGclamp(n.soma(0.5), dur, dt, ginh['mu'], ginh['sigma'], ginh['tau'], ginh['E'], delay=before, seed=rnd.poisson(10000))
    # recorders
    rec = {'t': h.Vector(), 'vsoma': h.Vector(), 'vais': h.Vector()}
    apc = nrn.makeAPcount(n.soma(0.5))
    spikes = h.Vector()
    apc.record(spikes)
    rec['t'].record(h._ref_t)
    rec['vsoma'].record(n.soma(0.5)._ref_v)
    #rec['vais'].record(n.axon['AIS'](0.5)._ref_v)
    spikes = h.Vector()
    apc.record(spikes)
    stim,vec = makeSine(n.soma(0.5), dur, dt, I0, I1, 0, f, 0, before)
    # run the simulation
    nrn.run(dur+before+after)
    import pylab as p
    p.plot(rec['t'],rec['vsoma'],'k',label='Soma')
    #p.plot(rec['t'],rec['vais'],'r',label='AIS')
    #p.legend(loc='best')
    p.show()
    return np.array(spikes) - before

def runCurrentModulationTrial(n, Imu, Isigma, Imod, f, dur, before):
    # additional simulation parameters
    dt = h.dt           # [ms]
    after = 100         # [ms]
    # temperature of the simulation
    h.celsius = 36      # [C]
    # recorders
    rec = {'t': h.Vector(), 'vsoma': h.Vector(), 'vais': h.Vector()}
    apc = nrn.makeAPcount(n.soma(0.5))
    spikes = h.Vector()
    apc.record(spikes)
    rec['t'].record(h._ref_t)
    rec['vsoma'].record(n.soma(0.5)._ref_v)
    rec['vais'].record(n.axon['AIS'](0.5)._ref_v)
    stim,vec = makeSine(n.soma(0.5), dur, dt, Imu, Isigma, Imod, f, 0, before)
    # run the simulation
    nrn.run(dur+before+after)
    import pylab as p
    p.plot(rec['t'],rec['vsoma'],'k',label='Soma')
    p.plot(rec['t'],rec['vais'],'r',label='AIS')
    p.legend(loc='best')
    p.show()
    return np.array(spikes) - before

def main():
    # neuron parameters
    L = 80              # [um]
    diam = 80           # [um]
    withIm = False
    withAxon = True
    # sinusoidal current parameters
    if withIm:
        n = AdaptingNeuron(L,diam,withAxon)
        Imu = 0.7           # [nA]
        Isigma = 0.15       # [nA]
    else:
        n = RegularSpikingNeuron(L,diam,withAxon)
        Imu = 0.4           # [nA]
        Isigma = 0.1        # [nA]
    Imod = 0.02             # [nA]
    Imod = 0
    Isigma = 0
    # simulation parameters
    dur = 1000        # [ms]
    before = 200        # [ms]
    # tested frequencies
    frequencies = np.array([0.1,0.2,0.5,1,2,5,10,20,50,100,200,300,500,600,1000])    # [Hz]
    spikes = dict.fromkeys(frequencies)
    for f in frequencies:
        print('F = %g Hz' % f)
        #spikes[f] = runCurrentModulationTrial(n, Imu, Isigma, Imod, f, dur, before)
        spikes[f] = runCurrentModulationTrialWithConductanceBG(n, R_exc=7000, Vbal=-50, I0=0, I1=Imod, f=f, dur=dur, before=before)

    saveMatlabFile('spikes_ap_peak.mat', spikes, before, dur)

if __name__ == '__main__':
    main()

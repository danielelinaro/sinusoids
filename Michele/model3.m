function out = model3(t, C, g, E, ge, Ee, gi, Ei, Io)
%
% MODEL2(t, C, g, E, ge, Ee, gi, Ei, Io)
%
% C dV/dt = g (E - V) + ge (Ee - V) + gi (Ei - V) + Io
%
% Michele Giugliano
%

G   = g + ge + gi;
EE  = (g*E + ge*Ee + gi*Ei)/G;
out = model1(t, C, G, EE, Io);

end

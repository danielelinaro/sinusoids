%% Initialization and parameters
clear all;
% close all;
clc;

%%
dt   = 0.1e-3;    % [s]
T    = 2.;        % [s]
t    = 0:dt:T;    % [s]
tau  = 20e-3;     % [s]
% C    = 1.;        % [uF/cm2]
E    = -65.;      % [mV]
Ee   = 0.;        % [mV]
Ei   = -80.;      % [mV]
taue = 5;         % [ms]
taui = 10;        % [ms]
Vbal = -10;       % [mV]
Rm   = 50;        % [MOhm]
re   = 7000;      % [Hz]
g    = 1e3/Rm;    % [nS]
C    = tau*g;     % [pF]

ratio = computeRatesRatio('relative',Vbal,Rm,taue,taui,Ee,Ei);
% ge and gi are in nS
[ge,gi] = computeSynapticBackgroundCoefficients('relative',Rm,re,ratio,taue,taui);
G = g + ge + gi;
EE = (g*E + ge*Ee + gi*Ei)/G;

%% Preparation of the inputs
%ind  = find(t>T/4.);
%I1(ind) = .5; % uA/cm2
%If(ind) = .5; % uA/cm2

%f1 = 10e-3;     f2 = 50e-3;     f3 = 100e-3;
%Io = 0.1*(0.3*sin(2*pi*f1*t) + 0.3*sin(2*pi*f2*t+0.2) + 0.3*sin(2*pi*f3*t+0.1));%1 * rand(size(t));
%If = Io;

rng(5061983);
Io  = randn(size(t));
tau = 10e-3;
AA  = [1, (dt/tau-1)];
BB  = [0, dt/tau];
Io = 1e3 * filter(BB, AA, Io);

%% Actual filtering of Io into If
% (If(k+1) - If(k)) = - dt*G/C* If(k) + (Io(k+1) - Io(k)) + dt*g/C*Io(k)
% If(k+1) = If(k) * (1-dt*G/C) + Io(k+1) - (1-dt*g/C) Io(k)
dt  = t(2) - t(1);
s = zpk('s');
sysc = (s*C+g)/(s*C+G);
sysd = c2d(sysc,dt);
[num,den] = tfdata(sysd);
AA = den{1};
BB = num{1};
If = filter(BB, AA, Io+G*EE) - g*E;

%% Plot the high-pass filter
f  = (1:0.1:1000.);   % [Hz]
s  = sqrt(-1)*2*pi*f;
figure;
semilogx(f, abs((s+g/C)./(s+G/C)));
xlabel('frequency [Hz]');
ylabel('Magnitude of the transfer function')

%% Computing the dynamical responses
out2 = model2(t, C, g, E, ge, Ee, gi, Ei, Io);      % Conductance-driven
out1 = model1(t, C, g, E, If);                      % Current-driven, corrected input
out3 = model1(t, C, g, E, Io);                      % Current-driven, same input as for the Conductance-driven

figure;
subplot(2,1,1)
P = plot(t, If, 'k',  t, Io, 'r');
xlabel('time [ms]');    ylabel('Signal [uA]');     title('Comparison');
legend([P(1) P(2)], 'High passed signal', 'signal');  
%ylim([-0.2 1.2]);
%ylim([-1 1]);
xlim([1.5,2])

subplot(2,1,2)
P = plot(t(1:25:end), out1(1:25:end), 'ko',  t, out2, 'r', t, out3, 'b');
xlabel('time [ms]');    ylabel('voltage [mV]');     title('Comparison');
legend([P(1) P(2) P(3)], 'Unbalanced (with HP input)', 'Balanced', 'Unbalanced');  
%ylim([-0.2 1.2]);
% ylim([-3 3]);
xlim([1.5,2])

function out = model2(t, C, g, E, ge, Ee, gi, Ei, Io)
%
% MODEL2(t, C, g, E, ge, Ee, gi, Ei, Io)
%
% C dV/dt = g (E - V) + ge (Ee - V) + gi (Ei - V) + Io
%
% Michele Giugliano
%

% V(k+1) = (1 - g dt / C) V(k) + (Io(k) + g E) dt / C 
dt  = t(2) - t(1);
G   = g + ge + gi;
EE  = (g*E + ge*Ee + gi*Ei)/G;

AA  = [1, (G*dt/C-1)];
BB  = [0, dt/C];
out = zeros(size(Io));
out = filter(BB, AA, Io+G*EE, EE);

end

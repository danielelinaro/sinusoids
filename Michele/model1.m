function out = model1(t, C, g, E, Io)
%
% MODEL1(t, C, g, E, Io)
%
% C dV/dt = g (E - V) + Io
%
% Michele Giugliano
%

% V(k+1) = (1 - g dt / C) V(k) + (Io(k) + g E) dt / C 
dt  = t(2) - t(1);
AA  = [1, (g*dt/C-1)];
BB  = [0, dt/C];
out = zeros(size(Io));
out = filter(BB, AA, Io+g*E, E);

end

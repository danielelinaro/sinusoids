%% Initialization and parameters
clear all;
close all;
clc;

dt = 0.1;       % [ms]
T  = 2000.;      % [ms]
t  = 0:dt:T;    % [ms]

C  = 1.;        % uF/cm2
g  = 0.05;      % mS/cm2
E  = -65.;      % mV

Ee = 0.;        % mV
Ei = -60.;      % mV
ge = g/10.;     % mS/cm2
gi = g/2;     % mS/cm2
G   = g + ge + gi;


%% Preparation of the inputs
Io   = zeros(size(t));      % External signal
If   = zeros(size(t));      % Its high-pass filtered version

%ind  = find(t>T/4.);
%I1(ind) = .5; % uA/cm2
%If(ind) = .5; % uA/cm2

%f1 = 10e-3;     f2 = 50e-3;     f3 = 100e-3;
%Io = 0.1*(0.3*sin(2*pi*f1*t) + 0.3*sin(2*pi*f2*t+0.2) + 0.3*sin(2*pi*f3*t+0.1));%1 * rand(size(t));
%If = Io;

Io  = randn(size(Io));
tau = 10.;
AA  = [1, (dt/tau-1)];
BB  = [0, dt/tau];
Io = filter(BB, AA, Io, 0);


%% Actual filtering of Io into If
% (If(k+1) - If(k)) = - dt*G/C* If(k) + (Io(k+1) - Io(k)) + dt*g/C*Io(k)
% If(k+1) = If(k) * (1-dt*G/C) + Io(k+1) - (1-dt*g/C) Io(k)
dt  = t(2) - t(1);
AA  = [1, (G*dt/C-1)];
BB  = [1, (dt*g/C-1)];
If = zeros(size(Io));
If = filter(BB, AA, Io,Io(1));

%% Plot the high-pass filter
f  = (1:0.1:300.);   % [Hz]
s  = sqrt(-1)*2*pi*f;
%H  = (s+g/C)./(s+G/C);
%H  = abs(H);
semilogx(f, abs((s+1e3*g/C)./(s+1e3*G/C)));
xlabel('frequency [Hz]');
ylabel('Magnitude of the transfer function')
%% Computing the dynamical responses
out2 = model2(t, C, g, E, ge, Ee, gi, Ei, Io);      % Conductance-driven
out1 = model1(t, C, g, E, If);                      % Current-driven, corrected input
out3 = model1(t, C, g, E, Io);                      % Current-driven, same input as for the Conductance-driven

%out1 = out1 - mean(out1(1:10));
%out2 = out2 - mean(out2(1:10));
out1 = out1 - mean(out1(end-2000:end));
out2 = out2 - mean(out2(end-2000:end));
out3 = out3 - mean(out3(end-2000:end));
%out1 = out1 / max(out1);
%out2 = out2 / max(out2);


subplot(2,1,1)
P = plot(t, If, 'k',  t, Io, 'r');
xlabel('time [ms]');    ylabel('Signal [uA]');     title('Comparison');
legend([P(1) P(2)], 'High passed signal', 'signal');  
%ylim([-0.2 1.2]);
%ylim([-1 1]);
xlim([1500 2000])

subplot(2,1,2)
P = plot(t(1:25:end), out1(1:25:end), 'ko',  t, out2, 'r', t, out3, 'b');
xlabel('time [ms]');    ylabel('voltage [mV]');     title('Comparison');
legend([P(1) P(2) P(3)], 'Unbalanced (with HP input)', 'Balanced', 'Unbalanced');  
%ylim([-0.2 1.2]);
ylim([-2 2]);
xlim([1500 2000])